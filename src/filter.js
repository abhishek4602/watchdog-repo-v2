import React, { Component,useState } from 'react';  
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { Platform, StyleSheet, View, Text,  

Image, TouchableOpacity, Alert, Picker ,CheckBox} from 'react-native';  
import { TouchableHighlight } from 'react-native-gesture-handler';
import 'react-native-gesture-handler';
 
 function Logs () {
    const onPress  = () =>{}
    const [isSelected, setSelection] = useState(false);
     
 
    
 
    var [ isPress, setIsPress ] = React.useState(false);
    var touchProps = {
        activeOpacity: 1,
        underlayColor: '#1c2833',                               // <-- "backgroundColor" will be always overwritten by "underlayColor"
        style: isPress ? styles.pressed : styles.nonPressed, // <-- but you can still apply other style changes
        onHideUnderlay: () => setIsPress(true),
        onShowUnderlay: () => setIsPress(false),
        onPress: () => setIsPress(false),                 // <-- "onPress" is apparently required
      };
  const [flatVisited, setflatVisited] = useState("");
    return(
        <View>
        <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:20,fontSize:15,marginTop:10,marginBottom:10}}>Visitor Logs</Text>  
        <View   style={{ flexDirection: 'row'}}>
              <Image 
               source={require('../assets/filter.png')} alignItems='center'
              style={{width:10,height:10,marginTop:2 ,marginStart:20 }}
              />
  
             <View>
             <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:10,marginTop:0,marginBottom:10,color:'#1c2833'}}>FILTERS</Text>  
             </View>
             <View>

             </View>
        </View>


        <View style={{ flexDirection: 'row'}}>

            <View style={{flex:1}}>

            <TouchableHighlight
            underlayColor='#1c2833'
            style={{
                
                borderColor:'#808B96',
               
                borderWidth:1,
                height:20,marginStart:20,marginBottom:12
            }}
            >
                           <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:10,marginTop:2,marginBottom:10,color:'#808B96'}}>FLAT NUMBER</Text>  
           
            </TouchableHighlight>
          
            </View>
            <View style={{flex:1}}>
            <TouchableHighlight  
            style={{
                borderColor:'#808B96',
               
                borderWidth:1,
                height:20,marginStart:5,marginBottom:12
            }}
             
            >
                          <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:10,marginTop:2,marginBottom:10,color:'#808B96'}}>DATE</Text>  
           
            </TouchableHighlight>
            <TouchableHighlight
            underlayColor='#1c2833'
            onPress={onPress}
           style={{
            borderColor:'#808B96',
         
            borderWidth:1,borderRadius:15,
            height:20,marginStart:15,marginBottom:12,marginEnd:25
        }}
        >
                          <Text style={{textAlign: 'center',fontFamily:'MS',marginStart:0,fontSize:10,marginTop:1,marginBottom:10,color:'#808B96'}}>Yesterday</Text>  
           
            </TouchableHighlight>
               
            <TouchableHighlight
            underlayColor='#1c2833'
            onPress={onPress}
           style={{
            borderColor:'#808B96',
         
            borderWidth:1,borderRadius:15,
            height:20,marginStart:15,marginBottom:12,marginEnd:25
        }}
        >
                          <Text style={{textAlign: 'center',fontFamily:'MS',marginStart:0,fontSize:10,marginTop:1,marginBottom:10,color:'#808B96'}}>Today</Text>  
           
            </TouchableHighlight>
            </View>
            <View style={{flex:1}}>
            <TouchableHighlight
            underlayColor='#1c2833'
            onPress={onPress}
           style={{
            borderColor:'#808B96',
         
            borderWidth:1,
            height:20,marginStart:5,marginBottom:12,marginEnd:20
        }}
        >
                          <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:10,marginTop:2,marginBottom:10,color:'#808B96'}}>VISITOR TYPE</Text>  
           
            </TouchableHighlight>
             
            <TouchableHighlight
            underlayColor='#1c2833'
            onPress={onPress}
           style={{
            borderColor:'#808B96',
         
            borderWidth:1,borderRadius:15,
            height:20,marginStart:15,marginBottom:12,marginEnd:25
        }}
        >
                          <Text style={{textAlign: 'center',fontFamily:'MS',marginStart:0,fontSize:10,marginTop:1,marginBottom:10,color:'#808B96'}}>Guest</Text>  
           
            </TouchableHighlight>
               
            <TouchableHighlight {...touchProps}
        //     underlayColor='#1c2833'
        //     onPress={onPress}
        //    style={{
        //     borderColor:'#808B96',
         
        //     borderWidth:1,borderRadius:15,
        //     height:20,marginStart:15,marginBottom:12,marginEnd:25
        // }}
        >
                          <Text style={{textAlign: 'center',fontFamily:'MS',marginStart:0,fontSize:10,marginTop:1,marginBottom:10,color:'#808B96'}}>Delivery</Text>  

            </TouchableHighlight>
               
            <TouchableOpacity {...touchProps}
        //     underlayColor='#1c2833'
        //     onPress={onPressSelect}
        //    style={{
        //     borderColor:'#808B96',
         
        //     borderWidth:1,borderRadius:15,
        //     height:20,marginStart:15,marginBottom:12,marginEnd:25
      //  }}
        >
                          <Text style={{textAlign: 'center',fontFamily:'MS',marginStart:0,fontSize:10,marginTop:1,marginBottom:10,color:'#808B96'}}>Services</Text>  
                       
            </TouchableOpacity>
            </View>
            
            
        </View>
        <View style={{ flexDirection: 'row'}}>
            <View>
            <Image 
               source={require('../assets/sort.png')} alignItems='center'
              style={{width:10,height:10,marginTop:2 ,marginStart:20 }}></Image>
            </View>
            <View>
            <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:10,marginTop:0,marginBottom:10,color:'#1c2833'}}>SORT</Text>  
            </View>


        </View>
        <View style={{ flexDirection: 'row'}}>
       <View>
            <TouchableHighlight
            underlayColor='#1c2833'
            onPress={onPress}
           style={{
            borderColor:'#808B96',
            width:120,
            borderWidth:1,
            height:20,marginStart:20,marginBottom:12
        }}
        >
                          <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:10,marginTop:2,marginBottom:10,color:'#808B96'}}>TIME:DESCENDING</Text>  
           
            </TouchableHighlight>

            </View>
            <View>
            <TouchableHighlight
            underlayColor='#1c2833'
            onPress={onPress}
           style={{
            borderColor:'#808B96',
            width:120,
            borderWidth:1,
            height:20,marginStart:5,marginBottom:12
        }}
        >
                          <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:10,marginTop:2,marginBottom:10,color:'#808B96'}}>TIME:ASCENDING</Text>  
           
            </TouchableHighlight>

            </View>
       </View>

        </View>
        
    );
}

const styles = StyleSheet.create({
  cardHeaderText:
  {
   fontFamily:'MS',
    margin:10,
    fontSize:30,    
   alignItems:'flex-start',
    color:"#D5D8DC",
    textAlign:'left'
  }, 
  nonPressed:
  {
    borderColor:'red',
    width:90,
    borderWidth:1,
    height:20,marginStart:5,marginBottom:12},

  pressed:
  {
  borderColor:'blue',
  width:90,
  borderWidth:1,
  borderRadius:12,
  height:20,marginStart:5,marginBottom:12}
});

export default Logs;