import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    FlatList,
    ActivityIndicator,
    TouchableOpacity,
    StatusBar,
  } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';
import moment from 'moment';
import React,{useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Logs from './filter'
import CardComponent from './flatListCardLayout'

function DetailsScreen({navigation}) {
    navigation.setOptions({headerShown: false});
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    useEffect(() => {
      fetch('https://watchdogdev.herokuapp.com/watchdog')
        .then((response) => response.json())
        .then((json) => setData(json))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
    }, []);
  
    return (
   
   
      <View style={{
         flex: 1, 
         padding: 5 , 
        // backgroundColor: '#rgba(223, 226, 235 ,0.5)'   
        backgroundColor:'#FFFFFF'
    }}
    
    >
   
  
      {isLoading ? <ActivityIndicator/> : (
        
    
        <FlatList
          data={data}
          ListHeaderComponent={<Logs></Logs>}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => (
            console.log(item),
            <TouchableOpacity style={{  height:80,
              margin:5,
              width:'95%',
              borderRadius:12,
              flex:1,
               alignItems:'center',backgroundColor: '#FFFFFF',
               // 46425D
              // backgroundColor: '#rgba(223, 226, 235 ,0.9)',
                shadowColor: '#000',
               shadowOffset: { width: 0, height: 2 },
               shadowOpacity: 0.3,
               shadowRadius: 2,
               elevation: 6, }}>
               
                    <View style={{flexDirection:'row'}}>
  
  
                    <View style={{flex:2,alignItems:'center'}}>  
                    <TouchableOpacity 
                    style={{
                      marginTop:12,
                      width:50,
                      height:50,
                      shadowColor: '#000',
               shadowOffset: { width: 0, height: 2 },
               shadowOpacity: 0.3,
               shadowRadius: 2,
               elevation: 6,
                     // backgroundColor:'#46425D',
                     backgroundColor:'#1c2833',  
                      borderRadius:30,
                      alignItems:'center',
                      alignContent:"center",
                    }}
                    >
                        <Image 
                               source={require('../assets/woman.png')}  
                               style={{width:40,height:40,marginTop:10  }}
                        />
                    </TouchableOpacity>
                    
                    </View>
  
                      <View style={{flex:4}}>
                
                    <Text style={styles.cardContentText}>{ item.nameVisitor }</Text>
                     <Text style={styles.cardContentTextLight}>Visited Flat {item.destinationFlatVisitor}</Text>
                     </View>
                     <View style={{flex:2}}>
                     <Image 
              source={require('../assets/clock.png')}  
             style={{width:15,height:15 ,marginTop:20,marginStart:20   }}
             />
                    <Text style={styles.cardContentTextLight}>{ item.timeStamp }</Text>
                    </View>
                   
                    </View>
  
                 
            </TouchableOpacity>
         
          )}
        />
      )}
    </View> 
    );
    
  }
  
const styles = StyleSheet.create({
    container: {
      flex: 1,
     // marginTop:45,
        backgroundColor: '#FFFFFF',
      
      alignItems: "flex-start",
      justifyContent: 'flex-start',
     // height:'50%'
    },
    splashContainer: {
      flex: 1,
     // marginTop:45,
        backgroundColor: '#FFFFFF',
      
      alignItems: 'center',
      justifyContent: 'flex-start',
     // height:'50%'
    },
    textStart: {
      flex: 1,
     // marginTop:45,
        backgroundColor: '#FFFFFF',
      
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
     // height:'50%'
    },
   
    inputBox:{
      borderWidth:1,
      borderRadius:5,
      //borderColor:'#A3E4D7',
      borderColor:'#D5D8DC',
      padding:10,
      margin:10,
      fontSize:12,
      width:'95%'
    },
    button: {
      flexDirection: 'row', 
      height: 50,
      borderRadius:10,
      width:'90%',
      backgroundColor: '#1c2833',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 10,
      marginBottom: 10,
      elevation:3,
   },
   buttonText: {
      fontSize: 20,
      fontFamily:'MS',
      color:'#FFFFFF'
   },
    headerText:
    {
      fontFamily:'MS',
      margin:10,
      fontSize:13,    
      marginStart:20,
      color:"#0F1139",
      
    },
   
    cardHText:
    {
      fontFamily:'MS',
      margin:10,
      fontSize:10,    
      marginStart:15,
      marginTop:-12,
      color:"#D6DBDF",
      
    },
    cardbackground:
    {
      height:100,
      margin:10,
      borderRadius:5,
      flex:1,
       alignItems:'center',
      backgroundColor:'rgba(247, 247, 247,0.8)',
      
    },
    cardHeaderbackground:
    {
      height:150,
      margin:10,
      width:'95%',
      borderRadius:5,
      flex:1,
       alignItems:'center',
      // backgroundColor:'#FDEDEC',
        backgroundColor:'#1c2833',
      
    },
    cardbackgroundPressed:
    {
      height:150,
      margin:10,
      borderRadius:5,
      flex:1,
      elevation:5,
      backgroundColor:'rgba(33,47,61,0.3)',
      
    },
    cardText:
    {
     fontFamily:'MS',
      margin:10,
      fontSize:10,    
      
      color:"#1C2833",
      textAlign:'center'
    },
    cardHeaderText:
    {
     fontFamily:'MS',
      margin:10,
      fontSize:30,    
     alignItems:'flex-start',
      color:"#D5D8DC",
      textAlign:'left'
    },
    cardContentText:{
      fontFamily:'Ubuntu-Bold',
      marginTop:15,
     marginStart:10,
      fontSize:14,    
     alignItems:'flex-start',
      color:"#1c2833",
      textAlign:'left'
    },
    cardContentTextLight:
    {
      fontFamily:'Ubuntu-Bold',
     
      margin:10,
      fontSize:10,    
     alignItems:'flex-start',
      color:"#808B96",
      textAlign:'left'
    }
   });
   
   
  export default DetailsScreen;