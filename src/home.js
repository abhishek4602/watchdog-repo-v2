
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
    ActivityIndicator,
    StatusBar,
    Picker,
  } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';
import moment from 'moment';
import React, {useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { TouchableHighlight } from 'react-native-gesture-handler';
//import _postData from './postData';



function HomeScreen({ navigation }) {
    navigation.setOptions({headerShown: false});
    const [flatVisited, setflatVisited] = useState("");
    const [visitorName, setvisitorName] = useState(''); 
    const [visitorTemperature, setvisitorTemperature] = useState('');
    const [vehicleNumber, setvehicleNumber] = useState('');
  return ( 
     
       <SafeAreaView  >
    <ScrollView   >
      <View style={styles.container}>
    <View style={{ flexDirection: 'row'}}>
    
    
  {/*     
      <Image  
             style={{flex:.5,width:150,height:150,marginTop:10}}
             source={require('./assets/authusericon.png')} alignItems='center'
    aaaa
           />
            */}
    <TouchableOpacity  style={{ flex:.1,right:10 }} activeOpacity={0.5}>
            
              <Image 
              onPress={() => i18n.changeLanguage('hi')}
                source={require('../assets/translate.png')} alignItems='center'
               style={{width:0,height:0,marginTop:10 ,marginStart:0 }}
              />
    
               
               </TouchableOpacity>
        
         </View>
    
            {/* <Card>
              <Text style={styles.headerText}>CardText</Text>
            </Card> */}
    
    <TouchableOpacity style={styles.cardHeaderbackground} >
    
   <View flexDirection='row'>
  
  <View style={{flex:6}}>
  <Image 
              source={require('../assets/building.png')} alignItems='center'
             style={{width:40,height:40,marginTop:40 ,marginStart:27 }}
            />
            <TouchableHighlight  
           underlayColor='#1c2833'
          
          style={{
           borderColor:'#808B96',
            width:80,
           borderWidth:1,borderRadius:0,
           height:20,marginStart:10,marginBottom:2,marginTop:10
       }}
       
            onPress={() => navigation.navigate('Details')}
            >
          <Text style={{textAlign: 'center',fontFamily:'MS',marginStart:0,fontSize:10,marginTop:1,marginBottom:10,color:'#808B96'}}>VIEW LOGS</Text>  
           
    
             
             </TouchableHighlight>
    
  </View>
     <View style={{flex:8}}>              
    <Text style ={styles.cardHeaderText}>Konark Towers</Text>
  <Text style ={styles.cardHText}>Wakad,Pune</Text>
    </View>
    <View style={{flex:4}}>
                
                <Text style ={styles.cardHeaderText}>21</Text>
             <Text style ={styles.cardHText}>Guests are yet to check OUT...</Text>
                </View>
   </View >
   
    </TouchableOpacity>
    <View style={styles.textStart}>
       <Text style={styles.headerText}>ENTER VISITORS NAME</Text>
          </View>
          
           <TextInput
               style={styles.inputBox}
               placeholder='Eg. Abhishek'  
               onChangeText={setvisitorName}
                />
     
                <Text style={styles.headerText}>ENTER VEHICLE NUMBER</Text>
               
           <TextInput
               style={styles.inputBox}
               placeholder='Eg. MH 12 JE 1292'
               onChangeText={setvehicleNumber}
                />
    
          <Text style={styles.headerText}>ENTER VISITOR'S TEMPERATURE</Text>
          
           <TextInput
                keyboardType={"number-pad"}
               style={styles.inputBox}
               placeholder='Eg.92 F'
               onChangeText={setvisitorTemperature}
                />
    
    
          <Text style={styles.headerText}>ENTER DESTINATION FLAT</Text>
          <Picker
           flatVisited={flatVisited}
           style={{ height: 50, width: '90%',marginStart:15}}
           onValueChange={(itemValue, itemIndex) => setflatVisited(itemValue)}> 
    
           <Picker.Item label="101" value="101" />
           <Picker.Item label="102" value="102" />
              <Picker.Item label="103" value="103" />
           <Picker.Item label="104" value="104" />
           <Picker.Item label="105" value="105" />
         </Picker>
          <Text style={styles.headerText}>Select Visitor Type</Text>
    
         <View style={{ flexDirection: 'row'}}>
    
         <TouchableOpacity style={styles.cardbackground}>
        {/* style={styles.cardbackground}>
         */}
         <Image 
              source={require('../assets/2.png')} alignItems='center'
              style={{width:60,height:60,marginTop:10 , }}
            />
            
            <Text style={styles.cardText}>HOUSEHELP</Text>
            
        </TouchableOpacity>
    
        <TouchableOpacity style={styles.cardbackground}>
           
        <Image 
              source={require('../assets/unnamed.png')} alignItems='center'
              style={{width:60,height:60,marginTop:10 , }}
            />
        <Text style={styles.cardText}>GUEST</Text>
    
    </TouchableOpacity>
    </View>
   
    
    
    <View style={{ flexDirection: 'row'}}>
    <TouchableOpacity activeOpacity={0.15} style={styles.cardbackground}>
     
     <Image 
               source={require('../assets/3.png')} alignItems='center'
              style={{width:60,height:60,marginTop:10 , }}
             />
     <Text style={styles.cardText}>RESIDENT</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.cardbackground}>
     
     <Image 
               source={require('../assets/delivery-boy.png')} alignItems='center'
               style={{width:60,height:60,marginTop:10 , }}
             />
     <Text style={styles.cardText}>DELIVERY</Text>
     </TouchableOpacity>
    </View>
        
    
      
        
                   <TouchableOpacity  
                   style={styles.button}
                   onPress={() => _postData(visitorName,flatVisited,vehicleNumber,visitorTemperature)}
                   >
                       <Text style={styles.buttonText}> SUBMIT </Text>
                   </TouchableOpacity>
                  
                  
             </View>
           </ScrollView>
       </SafeAreaView>
     );
    //ends here
  }
  

  const _postData = async (visitorName,flatVisited,vehicleNumber,visitorTemperature) =>{   

  
    var date = moment().utcOffset('+05:30').format(' hh:mm a');
    //var timeStamp = date.toLocaleTimeString();
    var details = {
      'nameVisitor': visitorName,
      'tempVisitor': visitorTemperature,
      'destinationFlatVisitor': flatVisited,
      'vehicleNumberVisitor': vehicleNumber,
      'timeStamp': date,
      'visitorType': 'Resident',
      };
  
  
       var formBody = [];
       for (var property in details) {
       var encodedKey = encodeURIComponent(property);
       var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");
  
      fetch('https://watchdogdev.herokuapp.com/putdata',{
  
      method:'POST',
      headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: formBody
      }).then((response) => response.json())
      .then((json) => {
       return json.movies;
      })
      .catch((error) => {
      console.error(error);
      });
  
  }

const styles = StyleSheet.create({
    container: {
      flex: 1,
     // marginTop:45,
        backgroundColor: '#FFFFFF',
      
      alignItems: "flex-start",
      justifyContent: 'flex-start',
     // height:'50%'
    },
    splashContainer: {
      flex: 1,
     // marginTop:45,
        backgroundColor: '#FFFFFF',
      
      alignItems: 'center',
      justifyContent: 'flex-start',
     // height:'50%'
    },
    textStart: {
      flex: 1,
     // marginTop:45,
        backgroundColor: '#FFFFFF',
      
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
     // height:'50%'
    },
   
    inputBox:{
      borderWidth:1,
      borderRadius:5,
      //borderColor:'#A3E4D7',
      borderColor:'#D5D8DC',
      padding:10,
      margin:10,
      fontSize:12,
      width:'95%'
    },
    button: {
      flexDirection: 'row', 
      height: 50,
      borderRadius:10,
      width:'90%',
      backgroundColor: '#1c2833',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 10,
      marginBottom: 10,
      marginStart:20,
      elevation:3,
   },
   buttonText: {
      fontSize: 20,
      fontFamily:'MS',
      color:'#FFFFFF'
   },
    headerText:
    {
      fontFamily:'MS',
      margin:10,
      fontSize:13,    
      marginStart:20,
      color:"#0F1139",
      
    },
   
    cardHText:
    {
      fontFamily:'MS',
      margin:10,
      fontSize:10,    
      marginStart:15,
      marginTop:-12,
      color:"#D6DBDF",
      
    },
    cardbackground:
    {
      height:100,
      margin:10,
      borderRadius:5,
      flex:1,
       alignItems:'center',
      backgroundColor:'rgba(247, 247, 247,0.8)',
      
    },
    cardHeaderbackground:
    {
      height:150,
      margin:10,
      width:'95%',
      borderRadius:5,
      flex:1,
       alignItems:'center',
      // backgroundColor:'#FDEDEC',
        backgroundColor:'#1c2833',
      
    },
    cardbackgroundPressed:
    {
      height:150,
      margin:10,
      borderRadius:5,
      flex:1,
      elevation:5,
      backgroundColor:'rgba(33,47,61,0.3)',
      
    },
    cardText:
    {
     fontFamily:'MS',
      margin:10,
      fontSize:10,    
      
      color:"#1C2833",
      textAlign:'center'
    },
    cardHeaderText:
    {
     fontFamily:'MS',
      margin:10,
      fontSize:30,    
     alignItems:'flex-start',
      color:"#D5D8DC",
      textAlign:'left'
    },
    cardContentText:{
      fontFamily:'Ubuntu-Bold',
      marginTop:15,
     marginStart:10,
      fontSize:14,    
     alignItems:'flex-start',
      color:"#1c2833",
      textAlign:'left'
    },
    cardContentTextLight:
    {
      fontFamily:'Ubuntu-Bold',
     
      margin:10,
      fontSize:10,    
     alignItems:'flex-start',
      color:"#808B96",
      textAlign:'left'
    }
   });
   
   
export default HomeScreen;